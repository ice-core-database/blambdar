# BLambdaR
This project is a fork of the "BLambdaR: REE patterns to shape coefficients" app made by Michael Anenburg (michael.anenburg@anu.edu.au), which you can access at https://lambdar.rses.anu.edu.au/blambdar/. 


## Description and Roadmap
Adjustments which will be made to the base code include:
 - normalization to an increased number of standards (e.g. upper crust)
 - comparision of changes to REE analyses through time
 - comparision of changes to REE analyses with different sample treatments (e.g. 1% nitric vs. 10% nitric)


## Usage
Please refer to the usage instructions provided for the originial app at https://lambdar.rses.anu.edu.au/codeb/instructions.html when attempting to upload data to this project. As a note, if an entire sample is missing, please insert non-zero numerical dummy data in the row as a place holder.


## Support
If you experience issues with the code, support can be sought by emailing hanna.brooks@maine.edu.

## Authors and Acknowledgement
Written by Hanna L Brooks. Last update: 2022.

Thanks to Michael Anenburg to all his work on the original app and scientific paper [Anenburg and Williams, 2022](https://link.springer.com/article/10.1007/s11004-021-09959-5)

## License
Code is licensed with a MIT License. See license section for more information.
